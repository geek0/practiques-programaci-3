// Copyright (C) Xavi Álvarez. All rights reserved.
package Exercici5;
import Keyboard.*;

public class Exercici5 {
	public static void main(String[] args) {
		char espai = ' ';
		char estrella = '*';
		System.out.print("Entra l'alçada del rombe: ");
		int n = Keyboard.readInt();
		while(n%2==0){
			System.out.println("Opció no vàlida, ha de ser senar.");
			System.out.print("Entra l'alçada del rombe: ");
			n = Keyboard.readInt();
		}
		  for (int i=1; i<=n; i++){ 
	           for (int espais = n - i; espais>0; espais--)       
	                System.out.print(espai); 
	                 for (int lineas = n; lineas < 2 * i; lineas++) 
	                   System.out.print(estrella); 
	                    System.out.println("");
	        }
	        for (int i=n; i>1; i--){ 
	        	if(i!=n){
	           for (int espais = n - i; espais>0; espais--)
	                System.out.print(espai); 
	                 for (int lineas = n; lineas < 2 * i; lineas++) 
	                   System.out.print(estrella); 
	                    System.out.println(""); 
	        	}
	        }
		}
} 
